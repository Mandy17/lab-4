#include "node.h"
#include <iostream>

// Take in value and create a node
node::node(int input)
{
    data = input;
    next = nullptr;
}
// Takes in an array of values and creates the appropriate nodes
node::node(int values[], int length)
{
    node* temp;
    temp = this;
    temp ->data = values[0];
    for(int i = 1; i<length; i ++){
        temp ->next = new node (values[i]);
        temp = temp ->next;
    }
}

// Default destructor (TA is going to write it)
node::~node()
{
    // Hint: You don't want to just delete the current node. You need to keep track of what is next
}

// Add a value to the end node
void node::append(int input)
{
    node* temp;
    temp = this;
    while (temp ->next != nullptr){
        temp = temp ->next;
    }
    temp ->next = new node (input);
}

// Add an array of values to the end as separate nodes
void node::append(int inputs[], int length)
{
    node* temp;
    temp = this;

    while (temp ->next != nullptr){
        temp = temp ->next;
    }
    for (int i = 0; i < length; i++){
        temp ->next = new node (inputs[i]);
        temp = temp ->next;
    }
}

//Insert a new node after the given location
node* node::insert(int location, int value) {

    node* temp;
    node* temp1 = new node (value);
    temp = this;

    for (int i = 1; i < location; i++){
        temp = temp ->next;
        //temp -> next = new node(location);
    }
    //temp ->next = new node (value);
      temp1 ->next = temp ->next; // review the drawing of the diagram
      temp ->next = temp1;

    return this; // Must return head pointer location
}
//---------------------------------------------------------------------------------------------------------------------------------
// Remove a node and link the next node to the previous node
node* node::remove(int location)
{
    node* temp;
    //node* temp1;
    temp = this;
    //temp1 = this;

    for (int i = 1; i < location; i++) { //need int i = 1 to get the correct array
        temp = temp ->next;
    }
    //temp ->data = temp ->next ->data;
    //temp1 ->next = temp ->next;
    temp ->next = temp ->next ->next;
    return this; // Must return head pointer location
}
//---------------------------------------------------------------------------------------------------------------------------------
// Print all nodes
void node::print()
{
    node* temp = this;
    while (temp ->next != nullptr){
       std:: cout << temp ->data << "";
        temp = temp -> next;
    }
   std:: cout << temp ->data << std:: endl;
}
//---------------------------------------------------------------------------------------------------------------------------------
//Print the middle node
void node::print_middle() {

    node* temp;
    node* runner1;
    node* runner2;
    temp = this;
    runner1 = this;
    runner2 = this;

    while (runner2 ->next != nullptr) {
        temp = runner1;
        runner1 = runner1 ->next;
        runner2 = runner2 ->next;

        if (runner2 ->next != nullptr){
            runner2 = runner2 ->next;

//        if ((temp1 ->next ->next) == nullptr) {
//            temp1 = temp1 ->next;
//        } else {
//            temp = temp ->next;
//            temp1 = temp1 ->next ->next;

            // HINT: Use a runner to traverse through the linked list at two different rates, 1 node per step
            //       and two nodes per step. When the faster one reaches the end, the slow one should be
            //       pointing to the middle
        }
    }
    std:: cout << temp ->data << std:: endl;

}
//---------------------------------------------------------------------------------------------------------------------------------
// Get the value of a given node
int node::get_value(int location) {

    node* temp;
    temp = this;

    for( int i = 0; i < location; i++){
        temp = temp ->next;
    }
    std:: cout << temp ->data << std:: endl;
}
//---------------------------------------------------------------------------------------------------------------------------------
// Overwrite the value of a given node
void node::set_data(int location, int value)
{
    node* temp;
    temp = this;

    for (int i = 0; i < location; i++){
        temp = temp ->next;
        //temp -> next = new node(location);
    }
    //std:: cout << temp ->data << std:: endl; this will not overwrite the value
    temp ->data = value;
}

// this has to be used
//temp = this; and after this will be pointing to this to the first node
//---------------------------------------------------------------------------------------------------------------------------------