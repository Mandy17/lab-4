#include <iostream>
#include "linked_list.h"
#include "node.h"

int main() {

    int m[5] = {1,6,3,4,5};
    linked_list o(m,5); // have to use linked_list or else it wont work

    int s[5] = {6,4,7,4,8};
    o.print();

    o.insert (5,4); //(location, number you want to insert)
    o.print();

    o.print_middle();
    o.print();

    o.append(s,5);
    o.print();

    o.remove(6);
    o.print();

    o.get_value(5);
    o.print();

    o.set_data(4,2); // (location, value)
    o.print();

    return 0;
}

//make a list of 5 and then inert the values.
//make list